//
//  ViewController.swift
//  liveness-demo-app
//
//  Created by Caio França on 18/06/24.
//

import UIKit
import BDCLivenessKit

class ViewController: UIViewController, BDCLivenessDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        BDCLivenessManager.shared.start(withToken: "", andDelegate: self)
    }

    func didCompleteLivenessSession(with result: BDCLivenessResult) {
        print(result)
    }
    
    func didFailLivenessSession(with error: BDCLivenessError) {
        print(error.description)
    }

}

